
"""
This file aims to clean the labels of tortured phrases by deleting the phrases contain AND operator,
and then lower all phrases.

!!!!! Need to add pathLabel and path_csv
"""

import pandas as pd
def main():

    # get the data after splitting 'AND' and 'OR' manually
    # pathLabel = '../data/torturedPhrasesClean.xlsx'
    pathLabel = ''
    dataLabel = pd.read_excel(pathLabel)
    # delete the first row
    dataLabel = dataLabel.loc[1:,]
    # get list if rows contains 'AND'
    list_idx_and = [idx for idx,label in enumerate(dataLabel.iloc[:,1])  if 'AND' in label]
    # delete rows contain 'AND'
    dataLabel = dataLabel.drop(dataLabel.index[list_idx_and])
    # strip "" and lower all the words
    dataLabel.iloc[:,1] = [fakeLabel.strip('"').lower() for fakeLabel in dataLabel.iloc[:,1]]
    # add column names
    dataLabel.columns = ['date', 'torturedPhrases', 'expectedPhrases', 'note','Internal use, do not touch']
    path_csv = ''
    # path = '../data/torturedPhrasesCleanNoAnd.csv'
    dataLabel.to_csv(path_csv)
    # save to csv

if __name__ == '__main__':
    main()
