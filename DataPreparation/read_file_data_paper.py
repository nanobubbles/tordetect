"""
This file aims to read each data file from the folder
"""

import torch
import os
import re

class ParaphraseDetectionDataset(torch.utils.data.Dataset):
    def __init__(self, data_dir):
        self.path_list = self.prepare_links(data_dir)

    def prepare_links(self, data_dir):
        path_list = []
        for filename in os.listdir(os.path.join(data_dir)):
            path_list.append(os.path.join(data_dir, filename))
        return path_list

    def __getitem__(self, idx):
        path = self.path_list[idx]
        with open(path, "r", encoding="'iso-8859-1'") as f:
            text = f.read()
        # keep only readable words
        return re.sub('[^A-Za-z.,;]+', ' ', text)

    def __len__(self):
        return len(self.path_list)



