# this file aims to get the clean data : status(yes/no), index_id, abstract, torturedPhrases, expectedPhrases
"""
    no paragraph : paragraph without tortured phrases
    yes paragraph : paragraph with tortured phrases
"""

import csv
import pandas as pd
from read_file_data_paper import *
import random
import os
import glob
import nltk
# nltk.download('punkt')
from nltk.tokenize import sent_tokenize

# path_clean_label = '/Users/neath/Google Drive (puthineathlay@gmail.com)/Intern M2/TorturedPhrases/data/torturedPhrasesCleanNoAnd.csv'
path_clean_label = '../data/torturedPhrases/torturedPhrasesCleanNoAnd.csv'
tools = ['spinbot','spinnerchief']
spinbot_folders = ['arxvip','machinep','machinep 2','thesis_par']
spinnerchief_folder = ['arxivp','arxivp2','thesisp','thesisp2','wikip','wikip2']

def read_path_abstract(tool,folder_name):
    # get datasets
    # path_abstract = f'/Users/neath/Documents/ParaphraseDetection2/data/automated_evaluation_up/{tool}/corpus/paragraphs/{folder_name}/mg'
    path_abstract = f'../data/automated_evaluation_up/{tool}/corpus/paragraphs/{folder_name}/mg'
     # read all file in the folder
    return ParaphraseDetectionDataset(path_abstract)

def checkPhrase(inputData,labels:list):
    # detect whether there are tortured phrases in the paragraph
    labelList = []
    for idx,abstract in enumerate(inputData):
        for label in labels:
            if label[0] in abstract:
        # if torturedPhrases in data_abstracts:
                list = [f'{idx}\t{abstract}\t{label[0]}\t{label[1]}']
                labelList.append(list)
    return labelList

def combined_abs_labels(tool,folder_name):
    """this function aims to combine abstracts and the detected labels"""
    # get the labels
    data_label = pd.read_csv(path_clean_label, sep=';')
    # get the dataset
    datasets = read_path_abstract(tool,folder_name)
    # get both label
    labels = list(zip(data_label['torturedPhrases'], data_label['expectedPhrases']))
    # match the para id in the folder to the detected phrases found in the paragraph
    found_phrases = checkPhrase(datasets, labels)
     # combined abstract and labels
    path_txt = f'../data/clean_data/yes/final_data_yes_{tool}-{folder_name}.txt'
    #
    with open(path_txt, 'w') as f:
        # header
        # f.write(f'status\tindex_id\tabstract\ttorturedPhrases\texpectedPhrases')
        for idx, phrases in enumerate(found_phrases):
            for elt in phrases:
                # print(elt)
                f.write(f'yes\t{elt}')
                f.write('\n')

def main():
    for tool in tools:
        if tool == 'spinbot':
            for folder in spinbot_folders:
                combined_abs_labels(tool,folder)
        elif tool == 'spinnerchief':
            for folder in spinnerchief_folder:
                combined_abs_labels(tool,folder)

#------------------Run the above code just to get the data with tortured phrases-------------------------

#------------------Below code aims to combined the abstract with and without tortured phrases---------------
def get_no_abstract(tool,folder_name):

    datasets = read_path_abstract(tool, folder_name)
    yes_data = pd.read_csv(f'../data/clean_data/yes/final_data_yes_{tool}-{folder_name}.txt',sep='\t')
    # get data from column ''index_id''
    index_id_list = [id for id in yes_data.iloc[:, 1]]
    # yes_data['index_id']
    index_id_no_list = [idx_id for idx_id, dataset in enumerate(datasets) if idx_id not in index_id_list ]
    # get the number of abstract without
    path_txt = f'../data/clean_data/no/final_data_no_{tool}-{folder_name}.txt'
    #
    with open(path_txt, 'w') as f:
        # f.write(f'status\tindex_id\tabstract\ttorturedPhrases\texpectedPhrases')
        # get the "no" paragraph equal to the number of the "yes" paragraph in each categories
        for id_no in random.choices(index_id_no_list, k=len(index_id_list)):
            f.write(f'no\t{id_no}\t{datasets[id_no]}')
            f.write('\n')

def main_no():
    for tool in tools:
        if tool == 'spinbot':
            for folder in spinbot_folders:
                get_no_abstract(tool, folder)
        elif tool == 'spinnerchief':
            for folder in spinnerchief_folder:
                get_no_abstract(tool, folder)

#------------------Below code aims to combined all files in multiple folders----------------

folders = ['no','yes']
def merge_files_folder(folder):
    path = '../data/clean_data/'
    # get all txt files in each folder
    files = glob.glob(path + folder + '/*.txt')
    # merge and save to "concat.txt" file
    with open(f"{path}{folder}/{folder}_concat.txt", "wb") as outfile:
        for f in files:
            with open(f, "rb") as infile:
                # next(f)
                outfile.write(infile.read())


def merge_txt_files():
    files = ['../data/clean_data/no/no_concat.txt', '../data/clean_data/yes/yes_concat.txt']
    output_path = '../data/clean_data/final_clean_data.txt'
    with open(output_path, 'w') as outfile:
        for fname in files:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

#------------------Below code aims to get the original data (without paraphraing)----------------

yes_data_list = ['arxvip','machinep','machinep 2','thesis_par']

def get_datasets(folder):
    path = f'../data/automated_evaluation_up/spinbot/corpus/paragraphs/{folder}/og'
    return ParaphraseDetectionDataset(path)

def get_original_data(yes_data_list:list):
    # get number(len) of yes data
    path_data_list = [f'../data/clean_data/yes/final_data_yes_spinbot-{yes_data}.txt' for yes_data in yes_data_list]
    count_yes = []
    for path_data in path_data_list:
        with open(path_data, 'r') as fp:
            for count, line in enumerate(fp):
                pass
            count_yes.append(count + 1)

    for yes_data, count_yes in zip(yes_data_list, count_yes):
        with open(f'../data/clean_data/no/final_data_no_original_{yes_data}.txt', 'w') as f:
            data_list = []
            for data in get_datasets(yes_data):
                # remove no_paragraph if the paragraph contains less than 50 tokens
                if len(nltk.word_tokenize(data)) > 50:
                    # select original data with the len equal to the yes data (data contains tortured phrases)
                    if len(data_list) == count_yes:
                        break
                    else:
                        data_list.append(data)

            for elt in data_list:
                f.write(f'no\t\t{elt}')
                f.write('\n')

# merge no_original data
def merge_no_data(filenames):
    path = '../data/clean_data/no/'
    path_data = [f'{path}final_data_no_original_{yes_data}.txt' for yes_data in filenames]

    with open(f'{path}no_original_concat.txt', 'w') as outfile:
        for fname in path_data:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

if __name__ == '__main__':

    # main_remove_short_para()

    merge_no_data(yes_data_list)

    # get_original_data(yes_data_list)

    # get_original_data(yes_data_list)

    # print(random.choices(get_datasets('arxvip'), k=5))

    # print(get_no_abstract())
    # print(type(test()))
    # main()
    # main_no()

    # for folder in folders:
    #     merge_files_folder(folder)

    # merge_txt_files()



