0	np	certain set	There is a certain set.	certain set	0.5193
0	np	certain set	Certain set.	certain set	0.5193
0	np	certain set	There's a certain set.	certain set	0.5193
0	np	certain set	Certain set	certain set	0.5193
0	np	certain set	It's a certain set.	certain set	0.5193
0	np	test function	It is a test function.	test function	0.3193
0	np	test function	The test function can be used.	test function	0.3193
0	np	test function	The test function can be used.	function used	0.4214
0	np	test function	The function is a test.	function test	0.3193
0	np	test function	It's a test function.	test function	0.3193
0	np	test function	There is a test function.	test function	0.3193
0	np	bounded operators	The operators are bounded.	operators bounded	0.2101
0	np	bounded operators	Operators that are bound.	operators bound	0.2226
0	np	bounded operators	Operators that are bounded.	operators bounded	0.2101
0	np	bounded operators	Operators that are bound by the law.	operators bound	0.2226
0	np	bounded operators	Operators that are bound by the law.	bound law	0.3234
0	np	bounded operators	The operators are bound.	operators bound	0.2226
0	np	many unbounded operators	Operators that are unbounded.	operators unbounded	0.2858
0	np	many unbounded operators	There are many operators that are unbounded.	many operators	0.3705
0	np	many unbounded operators	There are many operators that are unbounded.	operators unbounded	0.2858
0	np	many unbounded operators	There are many operators.	many operators	0.3705
0	np	many unbounded operators	Operators are unbounded.	operators unbounded	0.2858
0	np	many unbounded operators	Many operators are unbounded.	many operators	0.3705
0	np	many unbounded operators	Many operators are unbounded.	operators unbounded	0.2858
0	np	Wightman fields	The fields are owned by Wightman.	fields owned	0.3070
0	np	Wightman fields	The fields are owned by Wightman.	owned wightman	-0.0759
0	np	Wightman fields	The fields are by Wightman.	fields wightman	-0.0839
0	np	Wightman fields	The fields are owned by the Wightman family.	fields owned	0.3070
0	np	Wightman fields	The fields are owned by the Wightman family.	owned wightman	-0.0759
0	np	Wightman fields	The fields are owned by the Wightman family.	wightman family	-0.0833
0	np	Wightman fields	The fields of Wightman.	fields wightman	-0.0839
0	np	A Neumann algebra	A Neumann equation.	neumann equation	0.2578
0	np	A Neumann algebra	There is a Neumann equation.	neumann equation	0.2578
0	np	A Neumann algebra	A math problem.	math problem	0.2980
0	np	A Neumann algebra	There is a Neumann algebra.	neumann algebra	0.4667
0	np	A Neumann algebra	A Neumann equation	neumann equation	0.2578
0	np	inclusion order	There is an inclusion order.	inclusion order	0.3179
0	np	inclusion order	There is an order for inclusion.	order inclusion	0.3179
0	np	inclusion order	The order for inclusion.	order inclusion	0.3179
0	np	inclusion order	The inclusion order has been placed.	inclusion order	0.3179
0	np	inclusion order	The inclusion order has been placed.	order placed	0.4685
0	np	inclusion order	An order for inclusion.	order inclusion	0.3179
0	np	spacetime region	The time region.	time region	0.3811
0	np	spacetime region	There is a spacetime region.	spacetime region	0.0706
0	np	spacetime region	The region is called the spacetime region.	region called	0.4122
0	np	spacetime region	The region is called the spacetime region.	called spacetime	0.0282
0	np	spacetime region	The region is called the spacetime region.	spacetime region	0.0706
0	np	spacetime region	The spacetime region.	spacetime region	0.0706
0	np	spacetime region	The region is called spacetime.	region called	0.4122
0	np	spacetime region	The region is called spacetime.	called spacetime	0.0282
0	np	adjoint operation	adjoint operation	adjoint operation	-0.0279
0	np	adjoint operation	adjoint operation.	adjoint operation	-0.0279
0	np	adjoint operation	An adjoint operation.	adjoint operation	-0.0279
0	np	adjoint operation	The operation is adjoint.	operation adjoint	-0.0279
0	np	adjoint operation	There is an adjoint operation.	adjoint operation	-0.0279
0	np	space time	There is space time.	space time	0.4716
0	np	space time	There is time for space.	time space	0.4716
0	np	space time	It's space time.	space time	0.4716
0	np	space time	It is space time.	space time	0.4716
0	np	space time	There is time in space.	time space	0.4716
0	np	spectral projections	Projections of the stars.	projections stars	0.1252
0	np	spectral projections	Projections of light.	projections light	0.1894
0	np	spectral projections	Projections of the sun.	projections sun	0.0882
0	np	spectral projections	Projections of the sun's rays.	projections sun	0.0882
0	np	spectral projections	Projections of the sun's rays.	sun rays	0.3927
0	np	spectral projections	Projections on the ground.	projections ground	0.0722
0	np	mathematical object	There is a mathematical object.	mathematical object	0.3747
0	np	mathematical object	The object is a mathematical one.	object mathematical	0.3747
0	np	mathematical object	A mathematical object.	mathematical object	0.3747
0	np	mathematical object	The object is mathematical.	object mathematical	0.3747
0	np	mathematical object	There's a mathematical object.	mathematical object	0.3747
0	np	Wightman field	The field is called the Wightman field.	field called	0.3772
0	np	Wightman field	The field is called the Wightman field.	called wightman	-0.1011
0	np	Wightman field	The field is called the Wightman field.	wightman field	-0.0302
0	np	Wightman field	The field is called Wightman.	field called	0.3772
0	np	Wightman field	The field is called Wightman.	called wightman	-0.1011
0	np	Wightman field	There is a field called the Wightman field.	field called	0.3772
0	np	Wightman field	There is a field called the Wightman field.	called wightman	-0.1011
0	np	Wightman field	There is a field called the Wightman field.	wightman field	-0.0302
0	np	Wightman field	There is a field called Wightman.	field called	0.3772
0	np	Wightman field	There is a field called Wightman.	called wightman	-0.1011
0	np	Wightman field	The field is named after Wightman.	field named	0.3644
0	np	Wightman field	The field is named after Wightman.	named wightman	-0.0203
0	np	Neumann algebra	The Neumann equation.	neumann equation	0.2578
0	np	Neumann algebra	There is a Neumann equation.	neumann equation	0.2578
0	np	Neumann algebra	There is a Neumann algebra.	neumann algebra	0.4667
0	np	Neumann algebra	It's a Neumann equation.	neumann equation	0.2578
0	np	Neumann algebra	It's a Neumann problem.	neumann problem	0.1488
0	np	unbounded operator	The operator is unbounded.	operator unbounded	0.2750
0	np	unbounded operator	Operator that is unbounded.	operator unbounded	0.2750
0	np	unbounded operator	The operator was unbounded.	operator unbounded	0.2750
0	np	unbounded operator	Operator that is unbounded	operator unbounded	0.2750
0	np	unbounded operator	An operator that is unbounded.	operator unbounded	0.2750
0	np	expected properties	The properties are expected.	properties expected	0.2336
0	np	expected properties	There are expected properties.	expected properties	0.2336
0	np	expected properties	It is expected that the properties will be.	expected properties	0.2336
0	np	expected properties	The expected properties.	expected properties	0.2336
0	np	expected properties	It's expected properties.	expected properties	0.2336
0	np	Our mathematical aim	Our goal is mathematical.	goal mathematical	0.1495
0	np	Our mathematical aim	Our aim is mathematical.	aim mathematical	0.2281
0	np	Our mathematical aim	Our goal is mathematical	goal mathematical	0.1495
0	np	Our mathematical aim	Our goal is mathematics.	goal mathematics	0.1460
0	np	Our mathematical aim	Our aim is mathematics.	aim mathematics	0.2016
0	np	Hilbert space	There is a Hilbert space.	hilbert space	0.3650
0	np	Hilbert space	There is a space called the Hilbert space.	space called	0.3584
0	np	Hilbert space	There is a space called the Hilbert space.	called hilbert	0.1180
0	np	Hilbert space	There is a space called the Hilbert space.	hilbert space	0.3650
0	np	Hilbert space	The space is called the Hilbert space.	space called	0.3584
0	np	Hilbert space	The space is called the Hilbert space.	called hilbert	0.1180
0	np	Hilbert space	The space is called the Hilbert space.	hilbert space	0.3650
0	np	Hilbert space	There is a space called Hilbert space.	space called	0.3584
0	np	Hilbert space	There is a space called Hilbert space.	called hilbert	0.1180
0	np	Hilbert space	There is a space called Hilbert space.	hilbert space	0.3650
0	np	Hilbert space	There is a space called a Hilbert space.	space called	0.3584
0	np	Hilbert space	There is a space called a Hilbert space.	called hilbert	0.1180
0	np	Hilbert space	There is a space called a Hilbert space.	hilbert space	0.3650
0	np	A basic idea	There is a basic idea.	basic idea	0.4410
0	np	A basic idea	A basic idea.	basic idea	0.4410
0	np	A basic idea	The idea is a basic one.	idea basic	0.4410
0	np	A basic idea	An idea is a basic one.	idea basic	0.4410
0	np	A basic idea	An idea is a basic idea.	idea basic	0.4410
0	np	A basic idea	An idea is a basic idea.	basic idea	0.4410
0	np	bounded linear operators	Linear operators.	linear operators	0.3507
0	np	bounded linear operators	Linear operators are bound.	linear operators	0.3507
0	np	bounded linear operators	Linear operators are bound.	operators bound	0.2226
0	np	bounded linear operators	Linear operators are bounded.	linear operators	0.3507
0	np	bounded linear operators	Linear operators are bounded.	operators bounded	0.2101
0	np	bounded linear operators	The operators are linear.	operators linear	0.3507
0	np	bounded linear operators	Linear operators are in the category ofBounded Linear Operators.	linear operators	0.3507
0	np	bounded linear operators	Linear operators are in the category ofBounded Linear Operators.	operators category	0.1866
0	np	bounded linear operators	Linear operators are in the category ofBounded Linear Operators.	category ofbounded	0.0000
0	np	bounded linear operators	Linear operators are in the category ofBounded Linear Operators.	ofbounded linear	0.0000
0	np	bounded linear operators	Linear operators are in the category ofBounded Linear Operators.	linear operators	0.3507
0	np	physical consideration	The consideration is physical.	consideration physical	0.3279
0	np	physical consideration	It is a consideration of the physical.	consideration physical	0.3279
0	np	physical consideration	It is a physical consideration.	physical consideration	0.3279
0	np	physical consideration	There is a consideration of physical.	consideration physical	0.3279
0	np	physical consideration	The consideration of physical things.	consideration physical	0.3279
0	np	physical consideration	The consideration of physical things.	physical things	0.4449
0	np	bounded region	The region is large.	region large	0.4479
0	np	bounded region	The region is large	region large	0.4479
0	np	bounded region	The region is bounded.	region bounded	0.2916
0	np	bounded region	The region is very large.	region large	0.4479
0	np	spacetime regions	There are spacetime regions.	spacetime regions	0.0642
0	np	spacetime regions	Time regions.	time regions	0.2929
0	np	spacetime regions	The time regions.	time regions	0.2929
0	np	spacetime regions	There are regions of spacetime.	regions spacetime	0.0642
0	np	spacetime regions	There are space time regions.	space time	0.4716
0	np	spacetime regions	There are space time regions.	time regions	0.2929
0	np	unbounded operators	Operators that are unbounded.	operators unbounded	0.2858
0	np	unbounded operators	Operators are unbounded.	operators unbounded	0.2858
0	np	unbounded operators	Operators that are unbounded	operators unbounded	0.2858
0	np	unbounded operators	There are operators that are unbounded.	operators unbounded	0.2858
0	np	unbounded operators	The operators are unbounded.	operators unbounded	0.2858
0	np	strong operator topology	Operator topology is strong.	operator topology	0.2630
0	np	strong operator topology	Operator topology is strong.	topology strong	0.0298
0	np	strong operator topology	Operators are strong.	operators strong	0.1871
0	np	strong operator topology	Operators have a strong operator topology.	operators strong	0.1871
0	np	strong operator topology	Operators have a strong operator topology.	strong operator	0.1867
0	np	strong operator topology	Operators have a strong operator topology.	operator topology	0.2630
0	np	strong operator topology	Operators have a strong operator topology.	operators strong	0.1871
0	np	strong operator topology	Operators have a strong operator topology.	strong operator	0.1867
0	np	strong operator topology	Operators have a strong operator topology.	operator topology	0.2630
0	np	strong operator topology	The operator is strong.	operator strong	0.1867
