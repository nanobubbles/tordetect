import pandas as pd
from sklearn.model_selection import train_test_split
import torch
import transformers
import numpy as np
from torch.utils.data import Dataset, DataLoader
from transformers import RobertaTokenizer
from transformers import DistilBertModel
from transformers import RobertaConfig, RobertaModel
from transformers import Trainer, TrainingArguments
from sklearn.metrics import precision_recall_fscore_support, accuracy_score

# PRE_TRAINED_MODEL_NAME = 'distilbert-base-uncased'
PRE_TRAINED_MODEL_NAME = "roberta-base"



def prepare_data(data_one, data_zero):
  data = pd.concat([data_one, data_zero], axis=0).sample(frac = 1)
  x_train, x_test, y_train, y_test = train_test_split(data['sentence'], data['label'], test_size=0.33, random_state=42)
  return list(x_train), list(x_test), list(y_train), list(y_test)

class PostsDataset(Dataset):
    def __init__(self, posts, labels, tokenizer, max_len):
        # variables that are set when the class is instantiated
        self.posts = posts
        self.labels = labels

        self.tokenizer = tokenizer
        self.max_len = max_len

    def __len__(self):
        return len(self.posts)

    def __getitem__(self, item):
        # select the post and its category
        post = str(self.posts[item])
        label = self.labels[item]
        # tokenize the post
        tokenizer_out = self.tokenizer(
            post,
            add_special_tokens=True,
            max_length=self.max_len,
            return_token_type_ids=False,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt',
            truncation=True
        )
        # return a dictionary with the output of the tokenizer and the label
        return {
            'input_ids': tokenizer_out['input_ids'].flatten(),
            'attention_mask': tokenizer_out['attention_mask'].flatten(),
            'label': torch.tensor(label, dtype=torch.long)
        }

class RobertaClassification(RobertaModel):
    PRE_TRAINED_MODEL_NAME = "roberta-base"
    def __init__(self, config, num_labels, freeze_encoder=False):
        # instantiate the parent class DistilBertPreTrainedModel
        super().__init__(config)
        # instantiate num. of classes
        self.num_labels = num_labels
        # instantiate and load a pretrained DistilBERT model as encoder
        self.encoder = RobertaModel.from_pretrained(PRE_TRAINED_MODEL_NAME,add_pooling_layer=False)
        # freeze the encoder parameters if required (Q1)
        if freeze_encoder:
          for param in self.encoder.parameters():
              param.requires_grad = False
        # the classifier: a feed-forward layer attached to the encoder's head
        self.classifier = torch.nn.Linear(
            in_features=config.hidden_size, out_features=self.num_labels, bias=True)
        # in_features = 768, out_features = 4

        # instantiate a dropout function for the classifier's input
        self.dropout = torch.nn.Dropout(p=0.1)

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        head_mask=None,
        inputs_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
    ):
        # encode a batch of sequences with DistilBERT
        encoder_output = self.encoder(
            input_ids=input_ids,
            attention_mask=attention_mask,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
        )
        # extract the hidden representations from the encoder output
        hidden_state = encoder_output[0]  # (bs, seq_len, dim)
        # only select the encoding corresponding to the first token
        # of each sequence in the batch (Q2)
        pooled_output = hidden_state[:, 0]  # (bs, dim)
        # apply dropout
        pooled_output = self.dropout(pooled_output)  # (bs, dim)
        # feed into the classifier
        logits = self.classifier(pooled_output)  # (bs, dim)

        outputs = (logits,) + encoder_output[1:]

        if labels is not None: # (Q3)
          # instantiate loss function
          # SOLUTION :
          loss_fct = torch.nn.CrossEntropyLoss()
          # calculate loss
          # SOLUTION :
          loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
          # aggregate outputs
          outputs = (loss,) + outputs

        return outputs  # (loss), logits, (hidden_states), (attentions)

def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds)
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }

def main():
    # path = '/Users/neath/Documents/tordetect/data/clean_data/yes/5grams_sentence_yes.txt'
    path = '/home/sigma/layp/data/5grams_sentence_yes.txt'
    data = pd.read_csv(path, sep='\t')
    data = data.sample(frac=1)
    data = data[['sentence', 'label']]
    # replace label by number
    data['label'] = data['label'].replace(to_replace="yes", value=1)
    data['label'] = data['label'].replace(to_replace="no", value=0)
    data_one = data[data['label'] == 1]
    data_zero = data[data['label'] == 0]
    data20, data30, data50 = np.split(data_zero, [int(.2 * len(data_zero)), int(.5 * len(data_zero))])

    x_train, x_test, y_train, y_test = prepare_data(data_one, data30)

    MAX_LEN = 512

    # tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased', padding=True, truncation=True)
    tokenizer = RobertaTokenizer.from_pretrained("roberta-base")

    # instantiate two PostsDatasets
    train_dataset = PostsDataset(x_train, y_train, tokenizer, MAX_LEN)
    test_dataset = PostsDataset(x_test, y_test, tokenizer, MAX_LEN)



    # distilbert = DistilBertModel.from_pretrained(PRE_TRAINED_MODEL_NAME)
    my_roberta = RobertaModel.from_pretrained(PRE_TRAINED_MODEL_NAME)

    model = RobertaClassification(config=my_roberta.config, num_labels=2, freeze_encoder=True)

    training_args = TrainingArguments(
        output_dir='./results',
        logging_dir='./logs',
        logging_first_step=True,
        # logging_steps=50,
        evaluation_strategy="epoch",
        logging_strategy="epoch",
        num_train_epochs=1,
        per_device_train_batch_size=8,
        learning_rate=5e-5,
        # fp16=True

    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=test_dataset,
        compute_metrics=compute_metrics
    )

    train_results = trainer.train()
    test_results = trainer.predict(test_dataset=test_dataset)

    print('Predictions: \n', test_results.predictions)
    print('\nAccuracy: ', test_results.metrics['test_accuracy'])
    print('Precision: ', test_results.metrics['test_precision'])
    print('Recall: ', test_results.metrics['test_recall'])
    print('F1: ', test_results.metrics['test_f1'])
    # print(set(data['status']))

    MODEL_PATH = './my_model'
    trainer.save_model(MODEL_PATH)

if __name__ == '__main__':
    main()
