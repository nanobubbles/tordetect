import pandas as pd
import torch
from torch.utils.data import Dataset
from transformers import DistilBertModel, DistilBertPreTrainedModel, DistilBertTokenizer
import json
from transformers import Trainer, TrainingArguments
from sklearn.metrics import precision_recall_fscore_support, accuracy_score, classification_report

PRE_TRAINED_MODEL_NAME = 'distilbert-base-uncased'
MAX_LEN = 100
class PostsDataset(Dataset):
    def __init__(self, posts, labels, tokenizer, max_len):
        # variables that are set when the class is instantiated
        self.posts = posts
        self.labels = labels

        self.tokenizer = tokenizer
        self.max_len = max_len

    def __len__(self):
        return len(self.posts)

    def __getitem__(self, item):
        # select the post and its category
        post = str(self.posts[item])
        label = int(self.labels[item])
        # tokenize the post
        tokenizer_out = self.tokenizer(
            post,
            add_special_tokens=True,
            max_length=self.max_len,
            return_token_type_ids=False,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt',
            truncation=True
        )
        # return a dictionary with the output of the tokenizer and the label
        return {
            'input_ids': tokenizer_out['input_ids'].flatten(),
            'attention_mask': tokenizer_out['attention_mask'].flatten(),
            'label': torch.tensor(label, dtype=torch.long)
        }


class DistilBertForPostClassification(DistilBertPreTrainedModel):
    def __init__(self, config, num_labels, freeze_encoder=False):
        # instantiate the parent class DistilBertPreTrainedModel
        super().__init__(config)
        # instantiate num. of classes
        self.num_labels = num_labels
        # instantiate and load a pretrained DistilBERT model as encoder
        self.encoder = DistilBertModel.from_pretrained(PRE_TRAINED_MODEL_NAME)
        # freeze the encoder parameters if required (Q1)
        if freeze_encoder:
            for param in self.encoder.parameters():
                param.requires_grad = False
        # the classifier: a feed-forward layer attached to the encoder's head
        self.classifier = torch.nn.Linear(
            in_features=config.dim, out_features=self.num_labels, bias=True)
        # in_features = 768, out_features = 4

        # instantiate a dropout function for the classifier's input
        self.dropout = torch.nn.Dropout(p=0.1)

    def forward(
        self,
        input_ids=None,
        attention_mask=None,
        head_mask=None,
        inputs_embeds=None,
        labels=None,
        output_attentions=None,
        output_hidden_states=None,
    ):
        # encode a batch of sequences with DistilBERT
        encoder_output = self.encoder(
            input_ids=input_ids,
            attention_mask=attention_mask,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
        )
        # extract the hidden representations from the encoder output
        hidden_state = encoder_output[0]  # (bs, seq_len, dim)
        # only select the encoding corresponding to the first token
        # of each sequence in the batch (Q2)
        pooled_output = hidden_state[:, 0]  # (bs, dim)
        # apply dropout
        pooled_output = self.dropout(pooled_output)  # (bs, dim)
        # feed into the classifier
        logits = self.classifier(pooled_output)  # (bs, dim)

        outputs = (logits,) + encoder_output[1:]

        if labels is not None: # (Q3)
          # instantiate loss function
          loss_fct = torch.nn.CrossEntropyLoss()
          # calculate loss
          loss = loss_fct(logits.view(-1, self.num_labels), labels.view(-1))
          # aggregate outputs
          outputs = (loss,) + outputs

        return outputs  # (loss), logits, (hidden_states), (attentions)

def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds)
    acc = accuracy_score(labels, preds)
    report = classification_report(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall,
        'report':report
    }

def main():
    training_path = '/home/sigma/layp/data/splitting_train.csv'
    testing_path = '/home/sigma/layp/data/splitting_test.csv'
    train = pd.read_csv(training_path)
    test = pd.read_csv(testing_path)

    x_train, y_train, x_test, y_test = train['sentence'], train['label'], test['sentence'], test['label']
    # -----------------------------------------------------------------------------

    tokenizer = DistilBertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME, padding=True, truncation=True)

    distilbert = DistilBertModel.from_pretrained(PRE_TRAINED_MODEL_NAME)

    train_dataset = PostsDataset(x_train, y_train, tokenizer, MAX_LEN)
    test_dataset = PostsDataset(x_test, y_test, tokenizer, MAX_LEN)

    model = DistilBertForPostClassification(
        config=distilbert.config, num_labels=len(set(y_train)), freeze_encoder=True
    )
    learning_rate = 5e-5
    num_train_epochs = 1
    data_name = ''
    training_args = TrainingArguments(
        output_dir=f'./results_{data_name}_{learning_rate}_{num_train_epochs}e',
        logging_dir=f'./logs_{data_name}_{learning_rate}_{num_train_epochs}e',
        logging_first_step=True,
        logging_steps=100,
        # evaluation_strategy="epoch",
        # logging_strategy="epoch",
        num_train_epochs=num_train_epochs,
        per_device_train_batch_size=8,
        learning_rate=learning_rate,

    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=train_dataset,
        eval_dataset=test_dataset,
        compute_metrics=compute_metrics
    )

    train_results = trainer.train()
    test_results = trainer.predict(test_dataset=test_dataset)

    print('Predictions: \n', test_results.predictions)
    print('\nAccuracy: ', test_results.metrics['test_accuracy'])
    print('Precision: ', test_results.metrics['test_precision'])
    print('Recall: ', test_results.metrics['test_recall'])
    print('F1: ', test_results.metrics['test_f1'])
    print('classification_report',test_results.metrics['test_report'])


    MODEL_PATH = f'./my_model_{data_name}_{learning_rate}_{num_train_epochs}e'
    result_path = f'./results_{data_name}_{learning_rate}_{num_train_epochs}e/report.json'

    trainer.save_model(MODEL_PATH)
    with open(result_path,'w') as file:
        json.dump(test_results.metrics, file)

if __name__ == '__main__':
    main()

