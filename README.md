## Data

All data is in folder: 

    + data/automated_evaluation_up can be downloaded here: https://zenodo.org/record/3608000#.YwOCi-xBzRY

    + data/glove (glove.6b.zip) can be downloaded here: https://nlp.stanford.edu/projects/glove/

#### Data Preparation 
    + Data preparaton dolder : DataPreparation
#### Tortured phrases
    + original tortured phrases and prepared tortured phrases: data/torturedPhrases
#### Cosine score result
    + can be found here: data/cosine
#### All clean data preparation:
    + can be found here: data/clean_data
### * Collecting new data with cosine score (need to be continued):
    + can be found here: data/NewDataCollection/Paraphrasing_with_Transformers.ipynb
    + Name Entities and Noun Phrases extraction:
    Data extracted to new file (with the same name as original file) in "data/NewDataCollection/np_new_data_testing". 
    data column format: id paragraph(count manually from 0), type( ne/np), original phrase, paraphrased_np, bigram, bigram cosine score


## Cosine word
    + Cosine word computation of tutored phrases: 'ModelTesting/cosine_words.ipynb'
## Classification 
    + Classification experiment in: ModelTesting/Classification (except Roberta experiment is not mentioned in report
## Sentence Comparison
    + Sentence comparison : ModelTesting/SemanticComparison/compare_vector.ipynb

