# Training, and Report

## Download and prepare dataset
For this step you need the packages wget, unzip and tar.
```sh
sh prepare_data.sh /path/to/store/dataset
```

## Installing the requirements

```sh
pip install -r requirements.txt
````



